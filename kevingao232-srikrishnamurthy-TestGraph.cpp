// -------------
// TestGraph.c++
// -------------

// https://www.boost.org/doc/libs/1_73_0/libs/graph/doc/index.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// ---------
// TestGraph
// ---------

template <typename G>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = G;
    using vertex_descriptor   = typename G::vertex_descriptor;
    using edge_descriptor     = typename G::edge_descriptor;
    using vertex_iterator     = typename G::vertex_iterator;
    using edge_iterator       = typename G::edge_iterator;
    using adjacency_iterator  = typename G::adjacency_iterator;
    using vertices_size_type  = typename G::vertices_size_type;
    using edges_size_type     = typename G::edges_size_type;
};

// directed, sparse, unweighted
// possibly connected
// possibly cyclic
using
graph_types =
Types<
boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
Graph // uncomment
>;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, test0) {
using graph_type         = typename TestFixture::graph_type;
using vertex_descriptor  = typename TestFixture::vertex_descriptor;
using vertices_size_type = typename TestFixture::vertices_size_type;

graph_type g;

vertex_descriptor vdA = add_vertex(g);

vertex_descriptor vd = vertex(0, g);
ASSERT_EQ(vd, vdA);

vertices_size_type vs = num_vertices(g);
ASSERT_EQ(vs, 1u);
}

TYPED_TEST(GraphFixture, test1) {
using graph_type        = typename TestFixture::graph_type;
using vertex_descriptor = typename TestFixture::vertex_descriptor;
using edge_descriptor   = typename TestFixture::edge_descriptor;
using edges_size_type   = typename TestFixture::edges_size_type;

graph_type g;

vertex_descriptor vdA = add_vertex(g);
vertex_descriptor vdB = add_vertex(g);

edge_descriptor edAB = add_edge(vdA, vdB, g).first;

pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
ASSERT_EQ(p1.first,  edAB);
ASSERT_EQ(p1.second, false);

pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
ASSERT_EQ(p2.first,  edAB);
ASSERT_EQ(p2.second, true);

edges_size_type es = num_edges(g);
ASSERT_EQ(es, 1u);

vertex_descriptor vd1 = source(edAB, g);
ASSERT_EQ(vd1, vdA);

vertex_descriptor vd2 = target(edAB, g);
ASSERT_EQ(vd2, vdB);
}

TYPED_TEST(GraphFixture, test2) {
using graph_type        = typename TestFixture::graph_type;
using vertex_descriptor = typename TestFixture::vertex_descriptor;
using vertex_iterator   = typename TestFixture::vertex_iterator;

graph_type g;

vertex_descriptor vdA = add_vertex(g);
vertex_descriptor vdB = add_vertex(g);

pair<vertex_iterator, vertex_iterator> p = vertices(g);
vertex_iterator                        b = p.first;
vertex_iterator                        e = p.second;
ASSERT_NE(b, e);

vertex_descriptor vd1 = *b;
ASSERT_EQ(vd1, vdA);
++b;
ASSERT_NE(b, e);

vertex_descriptor vd2 = *b;
ASSERT_EQ(vd2, vdB);
++b;
ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test3) {
using graph_type        = typename TestFixture::graph_type;
using vertex_descriptor = typename TestFixture::vertex_descriptor;
using edge_descriptor   = typename TestFixture::edge_descriptor;
using edge_iterator     = typename TestFixture::edge_iterator;

graph_type g;

vertex_descriptor vdA = add_vertex(g);
vertex_descriptor vdB = add_vertex(g);
vertex_descriptor vdC = add_vertex(g);

edge_descriptor edAB = add_edge(vdA, vdB, g).first;
edge_descriptor edAC = add_edge(vdA, vdC, g).first;

pair<edge_iterator, edge_iterator> p = edges(g);
edge_iterator                      b = p.first;
edge_iterator                      e = p.second;
ASSERT_NE(b, e);

edge_descriptor ed1 = *b;
ASSERT_EQ(ed1, edAB);
++b;
ASSERT_NE(b, e);

edge_descriptor ed2 = *b;
ASSERT_EQ(ed2, edAC);
++b;
ASSERT_EQ(e, b);
}

TYPED_TEST(GraphFixture, test4) {
using graph_type          = typename TestFixture::graph_type;
using vertex_descriptor   = typename TestFixture::vertex_descriptor;
using adjacency_iterator  = typename TestFixture::adjacency_iterator;

graph_type g;

vertex_descriptor vdA = add_vertex(g);
vertex_descriptor vdB = add_vertex(g);
vertex_descriptor vdC = add_vertex(g);

add_edge(vdA, vdB, g);
add_edge(vdA, vdC, g);

pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
adjacency_iterator                           b = p.first;
adjacency_iterator                           e = p.second;
ASSERT_NE(b, e);

vertex_descriptor vd1 = *b;
ASSERT_EQ(vd1, vdB);
++b;
ASSERT_NE(b, e);

vertex_descriptor vd2 = *b;
ASSERT_EQ(vd2, vdC);
++b;
ASSERT_EQ(e, b);
}


TYPED_TEST(GraphFixture, test5) {
using graph_type          = typename TestFixture::graph_type;

graph_type g;

add_vertex(g);

ASSERT_EQ(num_vertices(g), 1);
}

TYPED_TEST(GraphFixture, test6) {
using graph_type          = typename TestFixture::graph_type;

graph_type g;

ASSERT_EQ(num_vertices(g), 0);
}

TYPED_TEST(GraphFixture, test7) {
using graph_type          = typename TestFixture::graph_type;

graph_type g;

for(int i = 0; i < 100; i++) {
add_vertex(g);
}
ASSERT_EQ(num_vertices(g), 100);
}

TYPED_TEST(GraphFixture, test8) {
using graph_type          = typename TestFixture::graph_type;
using vertex_descriptor  = typename TestFixture::vertex_descriptor;

graph_type g;

vertex_descriptor vdA = add_vertex(g);
vertex_descriptor vdB = add_vertex(g);


ASSERT_NE(vdA, vdB);
}

TYPED_TEST(GraphFixture, test9) {
using graph_type          = typename TestFixture::graph_type;
using vertex_descriptor  = typename TestFixture::vertex_descriptor;
using edge_descriptor   = typename TestFixture::edge_descriptor;

graph_type g;

vertex_descriptor vdA = add_vertex(g);
vertex_descriptor vdB = add_vertex(g);

pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);

ASSERT_EQ(p1.second, true);
}

TYPED_TEST(GraphFixture, test10) {
using graph_type          = typename TestFixture::graph_type;
using vertex_descriptor  = typename TestFixture::vertex_descriptor;
using edge_descriptor   = typename TestFixture::edge_descriptor;

graph_type g;

vertex_descriptor vdA = add_vertex(g);
vertex_descriptor vdB = add_vertex(g);

add_edge(vdA, vdB, g);
pair<edge_descriptor, bool> p2 = add_edge(vdA, vdB, g);

ASSERT_EQ(p2.second, false);
}

TYPED_TEST(GraphFixture, test11) {
using graph_type          = typename TestFixture::graph_type;
using vertex_descriptor  = typename TestFixture::vertex_descriptor;
using edge_descriptor   = typename TestFixture::edge_descriptor;

graph_type g;

vertex_descriptor vdA = add_vertex(g);
vertex_descriptor vdB = add_vertex(g);

edge_descriptor e1 = add_edge(vdA, vdB, g).first;
edge_descriptor eAB = edge(vdA, vdB, g).first;


ASSERT_EQ(e1, eAB);
}

TYPED_TEST(GraphFixture, test12) {
using graph_type          = typename TestFixture::graph_type;
using vertex_descriptor  = typename TestFixture::vertex_descriptor;
using edge_descriptor   = typename TestFixture::edge_descriptor;

graph_type g;

vertex_descriptor vdA = add_vertex(g);
vertex_descriptor vdB = add_vertex(g);
vertex_descriptor vdC = add_vertex(g);
vertex_descriptor vdD = add_vertex(g);

add_edge(vdA, vdB, g);
edge_descriptor e2 = add_edge(vdC, vdD, g).first;
edge_descriptor eAB = edge(vdA, vdB, g).first;


ASSERT_NE(e2, eAB);
}

TYPED_TEST(GraphFixture, test13) {
using graph_type          = typename TestFixture::graph_type;
using vertex_descriptor  = typename TestFixture::vertex_descriptor;


graph_type g;

vertex_descriptor vdA = add_vertex(g);
vertex_descriptor vdB = add_vertex(g);
vertex_descriptor vdC = add_vertex(g);
vertex_descriptor vdD = add_vertex(g);

add_edge(vdA, vdB, g);
add_edge(vdC, vdD, g);
add_edge(vdA, vdD, g);
add_edge(vdB, vdD, g);



ASSERT_EQ(num_edges(g), 4);
}

TYPED_TEST(GraphFixture, test14) {
using graph_type          = typename TestFixture::graph_type;
using vertex_descriptor  = typename TestFixture::vertex_descriptor;


graph_type g;

vertex_descriptor vdA = add_vertex(g);
vertex_descriptor vdB = add_vertex(g);
vertex_descriptor vdC = add_vertex(g);
vertex_descriptor vdD = add_vertex(g);

add_edge(vdA, vdB, g);
add_edge(vdC, vdD, g);


ASSERT_EQ(num_edges(g), 2);
}

TYPED_TEST(GraphFixture, test15) {
using graph_type          = typename TestFixture::graph_type;
using vertex_descriptor  = typename TestFixture::vertex_descriptor;
using edge_descriptor   = typename TestFixture::edge_descriptor;

graph_type g;

vertex_descriptor vdA = add_vertex(g);
vertex_descriptor vdB = add_vertex(g);


edge_descriptor e1 = add_edge(vdA, vdB, g).first;

ASSERT_EQ(source(e1, g), vdA);
}

TYPED_TEST(GraphFixture, test16) {
using graph_type          = typename TestFixture::graph_type;
using vertex_descriptor  = typename TestFixture::vertex_descriptor;
using edge_descriptor   = typename TestFixture::edge_descriptor;

graph_type g;

vertex_descriptor vdA = add_vertex(g);
vertex_descriptor vdB = add_vertex(g);


edge_descriptor e1 = add_edge(vdA, vdB, g).first;

ASSERT_EQ(target(e1, g), vdB);
}

TYPED_TEST(GraphFixture, test17) {
using graph_type          = typename TestFixture::graph_type;
using vertex_descriptor  = typename TestFixture::vertex_descriptor;
using edge_descriptor   = typename TestFixture::edge_descriptor;
using edge_iterator     = typename TestFixture::edge_iterator;

graph_type g;

vertex_descriptor vdA = add_vertex(g);
vertex_descriptor vdB = add_vertex(g);
vertex_descriptor vdC = add_vertex(g);
vertex_descriptor vdD = add_vertex(g);

edge_descriptor e1 = add_edge(vdA, vdB, g).first;
edge_descriptor e2 = add_edge(vdA, vdC, g).first;
edge_descriptor e3 = add_edge(vdA, vdD, g).first;
edge_descriptor e4 = add_edge(vdB, vdD, g).first;
edge_descriptor e5 = add_edge(vdC, vdD, g).first;

pair<edge_iterator, edge_iterator> temp = edges(g);
edge_iterator                      b = temp.first;
edge_iterator                      e = temp.second;
ASSERT_NE(b, e);

edge_descriptor eTemp1 = *b;
ASSERT_EQ(eTemp1, e1);
++b;

edge_descriptor eTemp2 = *b;
ASSERT_EQ(eTemp2, e2);
++b;

edge_descriptor eTemp3 = *b;
ASSERT_EQ(eTemp3, e3);
++b;

edge_descriptor eTemp4 = *b;
ASSERT_EQ(eTemp4, e4);
++b;

edge_descriptor eTemp5 = *b;
ASSERT_EQ(eTemp5, e5);
++b;

ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test18) {
using graph_type          = typename TestFixture::graph_type;
using vertex_descriptor  = typename TestFixture::vertex_descriptor;
using vertex_iterator   = typename TestFixture::vertex_iterator;

graph_type g;

vertex_descriptor vdA = add_vertex(g);
vertex_descriptor vdB = add_vertex(g);
vertex_descriptor vdC = add_vertex(g);
vertex_descriptor vdD = add_vertex(g);


pair<vertex_iterator, vertex_iterator> temp = vertices(g);
vertex_iterator                 b = temp.first;
vertex_iterator                   e = temp.second;
ASSERT_NE(b, e);

vertex_descriptor v1 = *b;
ASSERT_EQ(v1, vdA);
b++;

vertex_descriptor v2 = *b;
ASSERT_EQ(v2, vdB);
b++;

vertex_descriptor v3 = *b;
ASSERT_EQ(v3, vdC);
b++;

vertex_descriptor v4 = *b;
ASSERT_EQ(v4, vdD);
b++;


ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test19) {
using graph_type          = typename TestFixture::graph_type;
using vertex_descriptor  = typename TestFixture::vertex_descriptor;

graph_type g;

vertex_descriptor vdA = add_vertex(g);
vertex_descriptor vdB = add_vertex(g);

add_edge(vdA, 100, g);

ASSERT_EQ(num_vertices(g), 101);
}

TYPED_TEST(GraphFixture, test20) {
using graph_type          = typename TestFixture::graph_type;
using vertex_descriptor  = typename TestFixture::vertex_descriptor;
using edge_descriptor   = typename TestFixture::edge_descriptor;

graph_type g;

vertex_descriptor vdA = add_vertex(g);
vertex_descriptor vdB = add_vertex(g);

edge_descriptor e1 = add_edge(101, 100, g).first;

ASSERT_EQ(num_vertices(g), 102);

edge_descriptor eTest = edge(101, 100, g).first;
ASSERT_EQ(e1, eTest);
}
TYPED_TEST(GraphFixture, test21) {
using graph_type          = typename TestFixture::graph_type;
using vertex_descriptor  = typename TestFixture::vertex_descriptor;
using edge_descriptor   = typename TestFixture::edge_descriptor;

graph_type g;

ASSERT_EQ(num_edges(g), 0);
}

TYPED_TEST(GraphFixture, test22) {
using graph_type          = typename TestFixture::graph_type;
using vertex_descriptor  = typename TestFixture::vertex_descriptor;
using edge_descriptor   = typename TestFixture::edge_descriptor;

graph_type g;

add_edge(101, 100, g).first;
vertex_descriptor vdA = add_vertex(g);


ASSERT_EQ(vertex(102, g), vdA);
}

TYPED_TEST(GraphFixture, test23) {
using graph_type        = typename TestFixture::graph_type;
using vertex_descriptor = typename TestFixture::vertex_descriptor;
using edge_descriptor   = typename TestFixture::edge_descriptor;

graph_type g;

int N = 1000;

vector<vertex_descriptor> v;

for(int i = 0; i < N; i++) {
v.push_back(add_vertex(g));
}

for(int i = 0; i < N - 1; i++) {
vertex_descriptor a = v[i];
vertex_descriptor b = v[i+1];
ASSERT_EQ(add_edge(b, a, g).second, true);
ASSERT_EQ(edge(a, b, g).second, false);
}
}

TYPED_TEST(GraphFixture, test24) {
using graph_type        = typename TestFixture::graph_type;
using vertex_descriptor = typename TestFixture::vertex_descriptor;
using edge_descriptor   = typename TestFixture::edge_descriptor;

graph_type g;

int N = 1000;

vector<vertex_descriptor> v;

for(int i = 0; i < N; i++) {
v.push_back(add_vertex(g));
}

for(int i = 0; i < N - 1; i++) {
vertex_descriptor a = v[i];
vertex_descriptor b = v[i+1];
ASSERT_EQ(add_edge(a, b, g).second, true);
}
}

TYPED_TEST(GraphFixture, test25) {
using graph_type        = typename TestFixture::graph_type;
using vertex_descriptor = typename TestFixture::vertex_descriptor;
using edge_descriptor   = typename TestFixture::edge_descriptor;
using vertex_iterator   = typename TestFixture::vertex_iterator;
using edge_iterator   = typename TestFixture::edge_iterator;
using adjacency_iterator   = typename TestFixture::adjacency_iterator;

graph_type g;

int N = 1000;

vector<vertex_descriptor> v;

for(int i = 0; i < N; i++) {
v.push_back(add_vertex(g));
}

vector<edge_descriptor> e;

for(int i = 0; i < N - 1; i++) {
vertex_descriptor a = v[i];
vertex_descriptor b = v[i+1];
e.push_back(add_edge(a, b, g).first);
}

for(int i = 0; i < N - 1; i++) {
vertex_descriptor a = v[i];
vertex_descriptor b = v[i+1];
edge_descriptor ed = e[i];
pair<edge_descriptor, bool> res = edge(a, b, g);
ASSERT_EQ(ed, res.first);
ASSERT_EQ(res.second, true);
}
}

TYPED_TEST(GraphFixture, test26) {
using graph_type        = typename TestFixture::graph_type;
using vertex_descriptor = typename TestFixture::vertex_descriptor;
using edge_descriptor   = typename TestFixture::edge_descriptor;
using vertex_iterator   = typename TestFixture::vertex_iterator;
using edge_iterator   = typename TestFixture::edge_iterator;
using adjacency_iterator   = typename TestFixture::adjacency_iterator;

graph_type g;

int N = 1000;

vector<vertex_descriptor> v;

for(int i = 0; i < N; i++) {
v.push_back(add_vertex(g));
}

vector<edge_descriptor> e;
vector<edge_descriptor> e_rev;

for(int i = 0; i < N - 1; i++) {
vertex_descriptor a = v[i];
vertex_descriptor b = v[i+1];
e.push_back(add_edge(a, b, g).first);
e_rev.push_back(add_edge(b, a, g).first);
}

for(int i = 0; i < N - 1; i++) {
vertex_descriptor a = v[i];
vertex_descriptor b = v[i+1];
edge_descriptor ed = e[i];
edge_descriptor rev = e_rev[i];
pair<edge_descriptor, bool> res = edge(a, b, g);
pair<edge_descriptor, bool> res_rev = edge(b, a, g);
ASSERT_EQ(ed, res.first);
ASSERT_EQ(res.second, true);
ASSERT_EQ(rev, res_rev.first);
ASSERT_EQ(res_rev.second, true);
}
}

TYPED_TEST(GraphFixture, test27) {
using graph_type        = typename TestFixture::graph_type;
using vertex_descriptor = typename TestFixture::vertex_descriptor;
using edge_descriptor   = typename TestFixture::edge_descriptor;
using vertex_iterator   = typename TestFixture::vertex_iterator;
using edge_iterator   = typename TestFixture::edge_iterator;
using adjacency_iterator   = typename TestFixture::adjacency_iterator;

graph_type g;

int N = 1000;

vector<vertex_descriptor> v;

for(int i = 0; i < N; i++) {
v.push_back(add_vertex(g));
}

for(int i = 0; i < N; i++) {
vertex_descriptor a = v[i];
ASSERT_EQ(i, a);
}
}

TYPED_TEST(GraphFixture, test28) {
using graph_type        = typename TestFixture::graph_type;
using vertex_descriptor = typename TestFixture::vertex_descriptor;
using edge_descriptor   = typename TestFixture::edge_descriptor;
using vertex_iterator   = typename TestFixture::vertex_iterator;
using edge_iterator   = typename TestFixture::edge_iterator;
using adjacency_iterator   = typename TestFixture::adjacency_iterator;

graph_type g;

vertex_descriptor a = add_vertex(g);
pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(a, g);
ASSERT_EQ(p.first, p.second);
}


TYPED_TEST(GraphFixture, test29) {
using graph_type        = typename TestFixture::graph_type;
using vertex_descriptor = typename TestFixture::vertex_descriptor;
using edge_descriptor   = typename TestFixture::edge_descriptor;
using vertex_iterator   = typename TestFixture::vertex_iterator;
using edge_iterator   = typename TestFixture::edge_iterator;
using adjacency_iterator   = typename TestFixture::adjacency_iterator;

graph_type g;

vertex_descriptor a = add_vertex(g);
vertex_descriptor b = add_vertex(g);
add_edge(a, b, g);

pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(a, g);
auto begin = p.first;
auto end = p.second;
while(begin != end) {
vertex_descriptor v = *begin;
ASSERT_EQ(v, b);
++begin;
}
ASSERT_EQ(begin, end);
}

TYPED_TEST(GraphFixture, test30) {
using graph_type        = typename TestFixture::graph_type;
using vertex_descriptor = typename TestFixture::vertex_descriptor;
using edge_descriptor   = typename TestFixture::edge_descriptor;
using vertex_iterator   = typename TestFixture::vertex_iterator;
using edge_iterator   = typename TestFixture::edge_iterator;
using adjacency_iterator   = typename TestFixture::adjacency_iterator;

graph_type g;

int N = 1000;

vector<vertex_descriptor> v;

for(int i = 0; i < N; i++) {
v.push_back(add_vertex(g));
}

vector<edge_descriptor> e;

for(int i = 0; i < N - 1; i++) {
vertex_descriptor a = v[i];
vertex_descriptor b = v[i+1];
e.push_back(add_edge(a, b, g).first);
}

for(int i = 0; i < N - 1; i++) {
vertex_descriptor a = v[i];
vertex_descriptor b = v[i+1];
pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(a, g);
auto begin = p.first;
auto end = p.second;
while(begin != end) {
vertex_descriptor temp = *begin;
ASSERT_EQ(temp, b);
++begin;
}
ASSERT_EQ(begin, end);
}
}



