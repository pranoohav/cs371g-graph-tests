// -------------
// TestGraph.c++
// -------------

// https://www.boost.org/doc/libs/1_73_0/libs/graph/doc/index.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// ---------
// TestGraph
// ---------

template <typename G>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = G;
    using vertex_descriptor   = typename G::vertex_descriptor;
    using edge_descriptor     = typename G::edge_descriptor;
    using vertex_iterator     = typename G::vertex_iterator;
    using edge_iterator       = typename G::edge_iterator;
    using adjacency_iterator  = typename G::adjacency_iterator;
    using vertices_size_type  = typename G::vertices_size_type;
    using edges_size_type     = typename G::edges_size_type;};

// directed, sparse, unweighted
// possibly connected
// possibly cyclic
using
    graph_types =
    Types<
            //boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
            Graph // uncomment
            >;

#ifdef __APPLE__
    TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
    TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);}

TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);}

TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);}

TYPED_TEST(GraphFixture, test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);}

    //---------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------
    TYPED_TEST(GraphFixture, add_edge1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    auto answer = add_edge(vdA, vdB, g);
    ASSERT_TRUE(answer.second);
}

TYPED_TEST(GraphFixture, add_edge2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    auto a = add_edge(vdA, vdB, g);
    ASSERT_TRUE(a.second);
    auto b = add_edge(vdA, vdB, g);
    ASSERT_FALSE(b.second);
}

TYPED_TEST(GraphFixture, add_edge3) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;
    
    // allow cyclic graph
    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    const auto a = add_edge(vdA, vdB, g);
    ASSERT_TRUE(a.second);
    const auto b = add_edge(vdB, vdA, g);
    ASSERT_TRUE(b.second);
}

TYPED_TEST(GraphFixture, add_edge4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;
    
    // allow cyclic graph
    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    auto [edged, complete] = add_edge(vdA, vdB, g);
    ASSERT_TRUE(complete);
    ASSERT_TRUE(edge(vdA, vdB, g).second);
    ASSERT_EQ(edged, edge(vdA, vdB, g).first);
}

TYPED_TEST(GraphFixture, add_vertex1) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 1);
}

TYPED_TEST(GraphFixture, add_vertex2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    ASSERT_EQ(num_vertices(g), 2);
    ASSERT_NE(vdA, vdB);
}

TYPED_TEST(GraphFixture, add_vertex3) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);

    ASSERT_EQ(num_vertices(g), 4);
    ASSERT_NE(vdA, vdB);
}

TYPED_TEST(GraphFixture, adjacent_vertices1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
        
    auto [a, b] = adjacent_vertices(vdA, g);
    ASSERT_TRUE(a == b);
}

TYPED_TEST(GraphFixture, adjacent_vertices2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    add_edge(vdA, vdB, g);    
    auto [a, b] = adjacent_vertices(vdA, g);
    ASSERT_TRUE(b != a);
    ASSERT_EQ(std::distance(a, b), 1);
}

TYPED_TEST(GraphFixture, adjacent_vertices3) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    add_edge(vdA, vdB, g);    
    auto [a, b] = adjacent_vertices(vdB, g);
    ASSERT_TRUE(a == b);
    ASSERT_EQ(std::distance(a, b), 0);
}

TYPED_TEST(GraphFixture, edge1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    auto [initial, completed] = add_edge(vdA, vdB, g);
    auto [second, present] = edge(vdA, vdB, g);
    ASSERT_TRUE(present);
    ASSERT_EQ(initial, second);
}

TYPED_TEST(GraphFixture, edge2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);    

    auto [edged, present] = edge(vdA, vdB, g);
    ASSERT_FALSE(present);
}

TYPED_TEST(GraphFixture, edges1) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);

    auto [a, b] = edges(g);
    ASSERT_TRUE(a == b);
    ASSERT_EQ(std::distance(a, b), 0);
}

TYPED_TEST(GraphFixture, edges2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    add_edge(vdA, vdB, g);
    auto [a, b] = edges(g);
    ASSERT_FALSE(a == b);
    ASSERT_EQ(std::distance(a, b), 1);
}

TYPED_TEST(GraphFixture, edges3) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    add_edge(vdA, vdB, g);
    add_edge(vdA, vdB, g);
    add_edge(vdB, vdC, g);
    auto [a, b] = edges(g);
    ASSERT_FALSE(a == b);
    ASSERT_TRUE(num_vertices(g) == 3);
    ++a;
    ASSERT_FALSE(a == b);
}

TYPED_TEST(GraphFixture, num_edges1) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    ASSERT_EQ(num_edges(g), 0);
}

TYPED_TEST(GraphFixture, num_edges2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdB, g);
    ASSERT_EQ(num_edges(g), 1);
}

TYPED_TEST(GraphFixture, num_edges3) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(num_edges(g), 0);
}

TYPED_TEST(GraphFixture, num_edges4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    for (int i = 0; i < 10; ++i) {

        add_edge(vdA, vdB, g);
    }
    ASSERT_EQ(num_edges(g), 1);
}

TYPED_TEST(GraphFixture, num_vertices1) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    ASSERT_EQ(num_vertices(g), 0);
}

TYPED_TEST(GraphFixture, num_vertices2) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);

    ASSERT_EQ(num_vertices(g), 1);
}

TYPED_TEST(GraphFixture, num_vertices3) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    for (int i = 0; i < 10; ++i) {
        add_vertex(g);
    }

    ASSERT_EQ(num_vertices(g), 10);
}

TYPED_TEST(GraphFixture, source1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    auto [edged, c] = add_edge(vdA, vdB, g);
    ASSERT_EQ(source(edged, g), vdA);
}

TYPED_TEST(GraphFixture, source2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    auto [edged, c] = add_edge(vdC, vdB, g);
    ASSERT_EQ(source(edged, g), vdC);
}

TYPED_TEST(GraphFixture, target1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    auto [edged, c] = add_edge(vdA, vdB, g);
    ASSERT_EQ(target(edged, g), vdB);
}

TYPED_TEST(GraphFixture, target2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    auto [edged, c] = add_edge(vdA, vdC, g);
    ASSERT_EQ(target(edged, g), vdC);
}

TYPED_TEST(GraphFixture, vertex1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor v0  = vertex(0, g);
    ASSERT_FALSE(vdA == v0 && vdB == v0);
}

TYPED_TEST(GraphFixture, vertices1) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    auto [a, b] = vertices(g);
    ASSERT_TRUE(a == b);
    ASSERT_EQ(std::distance(a, b), 0);
}

TYPED_TEST(GraphFixture, vertices2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    add_vertex(g);

    auto [a, b] = vertices(g);
    ASSERT_FALSE(a == b);
    ASSERT_EQ(std::distance(a, b), 1);
}

TYPED_TEST(GraphFixture, vertices3) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);    
    auto [b, e] = vertices(g);
    ASSERT_FALSE(b == e);
    ASSERT_EQ(num_vertices(g), 3);    
}
    